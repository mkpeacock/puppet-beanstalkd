class beanstalkd {

    package{ "beanstalkd":
        ensure => present
    }
    
    file{ "/etc/default/beanstalkd":
    	owner  => root,
    	group  => root,
    	mode   => '0444',
    	source => "puppet:///modules/beanstalkd/config",
    	require => Package['beanstalkd'],
    	notify => Service['beanstalkd']
    }
    
    service{"beanstalkd":
    	ensure => "running",
    	enable => "true",
    	require => Package['beanstalkd']
    }
    
}